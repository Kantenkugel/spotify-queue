# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
 - Added dark theme and mode switch.
 - Added support for playlists:
   - You can search for playlists by prefixing the search with `pl:` or entering their link/uri.
   - A short info containing playlist name, owner of playlist and number of tracks will be shown and the possibility to add the playlist to the queue (max 100 tracks).
 - Added some contextual info for action buttons which will profit screen readers.
 - Now showing size of queue in parenthesis.
 - Tracks in the search results now indicate whether or not this track is already in the queue and at what position.
 - Server and client time should now be synchronized (better).
 - (API) Added docs to the WebSocket API.

### Changed
 - In order to make space for dark mode switch, the refresh button was switched to the queue card.
 - When searching, the current state is now properly displayed (searching vs nothing found).
 - Optimized (network) performance by using smallest, still fitting images.
 - Switched order of queue and search sections to find the search bar easier.

### Deprecated
 - (API) Deprecated use of mass add event for playlist. Use the new `addMore` event. In the meantime, they will still be fired, with an additional `isBulk` boolean flag.

### Fixed
 - Current playback time will now just show as 0:00 when time is < 0 (slight time-diff of server and client).

## [1.1.0] - 2017-12-13
### Added
 - When submitting the search form, the search input field will now lose focus. This is nice when using phones and hitting "enter".
 - You can now enter spotify links or uris into the search bar. This will show the track as search result to be added to the queue.
 - The client will now periodically (every 2s) try to connect/reconnect to the server when the connection is lost.
 - Added refresh button in navigation bar to manually refresh track+queue.

### Changed
 - Moved clear button of search inside of the result block. This leaves more room for the search button on small devices (phones).

### Fixed
 - The first entry of the queue can now properly be removed.

## [1.0.0] - 2017-12-12
### Added
#### Client
 - Graphical control panel written with React
 - Connects to server via WebSocket
 - Allows for
   - Searching tracks
   - Queueing searched tracks
   - Removing tracks from queue
   - Play/Pause/Skip currently playing track

#### Server
 - Connects to Spotify Web API and Spotify's built-in webserver
 - Has all the queue logic
 - Provides API through WebSocket
 - Serves client website through built-in webserver