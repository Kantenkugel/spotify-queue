# Spotify-DJ API
This page describes the api that is used for the server <=> client communication of Spotify-DJ.

## Quick links
 - [Introduction](#introduction)
 - [Commands](#commands)
 - [Events](#events)
 - [Entities](#entities)

## Introduction
Spotify-DJ uses a webserver on port `81` to both serve the client application and provide its API via WebSocket, which is used by the client.

If you want to create your own client, you just need to connect to the WebSocket (on port `81`) and you are ready to receive events and send commands.

## WebSocket protocol
The WebSocket server will reject all clients who provide an incorrect protocol (set) on connection. 

Protocol(s) used:

| Protocol        | Status    |
|:--------------- |:--------- |
| spotifyQueue-1  | current   |

## WebSocket messages
All WebSocket messages (sent/accepted from server) are json-encoded objects described in the sections below.

## Commands
Commands are messages sent from the client to the server and always include the field `cmd` to indicate the command to execute. Additional fields sent are described in the corresponding commands' docs.

The servers response will always be of the form

| Field  | Description                                                         |
|:------ |:------------------------------------------------------------------- |
| type   | the type of response. Possible values are `ok` and `error`.         |
| cmd    | The same cmd value used in the command request sent to the server.  |
| error  | Only present if the response is of type `error`, It contains the error message as string. |
| body   | Only present if the response is of type `ok`. It contains the response of the actual command (described in the corresponding command section) and may be omitted if the command does not return anything. |

### Get
This command will retrieve the current status of the currently playing track and the queue and is the preferred way to initializing the client.

#### Command parameters:
| Field  | Value |
|:------ |:----- |
| cmd    | `get` |

#### Command response (if `ok`):
| Field   | Description                                    |
|:------- |:---------------------------------------------- |
| current | The current track as [Rich Track](#rich-track) |
| queue   | The current [Queue](#queue)                    |

### Sync
Used to synchronize client and server clock and is done when initializing the client / clicking refresh.

#### Command parameters:
| Field  | Value  |
|:------ |:------ |
| cmd    | `sync` |

#### Command response (if `ok`):
The server-side timestamp (ms)

### Search
This command will issue the server to search for a track or playlist on the Spotify API and return up to the first `20` results.

#### Command parameters:
| Field  | Value    |
|:------ |:-------- |
| cmd    | `search` |
| query  | The query to search for. This can either be the URI or link to a Spotify track or playlist, or a search string (prefixed with `pl:` to search for playlists instead of tracks). Additional options of the search string are described in [Spotify's Web API](https://developer.spotify.com/web-api/search-item/) |

#### Command response (if `ok`):
Array of [Track(s)](#track) or [Playlist(s)](#playlist)

### Queue (Command)
This command will queue up the given track or playlist.

#### Command parameters:
| Field  | Value    |
|:------ |:-------- |
| cmd    | `queue`  |
| uri    | The URI of the track/playlist to queue. This is always included in the [Track](#track) and [Playlist](#playlist) objects returned from the search command |

#### Command response (if `ok`):
The [Track](#track) or [Playlist](#playlist) that was queued.


### Skip
This command is used to skip the currently playing track and play the next one in the queue instead.

#### Command parameters:
| Field  | Value    |
|:------ |:-------- |
| cmd    | `skip`   |

#### Command response (if `ok`):
This command doesn't return anything on success.

### Pause
Used to pause/unpause the current track.

#### Command parameters:
| Field  | Value    |
|:------ |:-------- |
| cmd    | `pause`  |
| value  | Optional boolean indicating the new pause value (`true` = paused, `false` = unpaused). If omitted, pause status will be toggled

#### Command response (if `ok`):
This command doesn't return anything on success.

### Remove
Removes the specified track from the queue.

#### Command parameters:
| Field  | Value                          |
|:------ |:------------------------------ |
| cmd    | `remove`                       |
| uri    | the URI of the track to remove |
| index  | The expected index inside of the queue where the track should be deleted at. The server will check a few indexes lower as well in case the index changed right before the command call (skip, track end, other remove) |

#### Command response (if `ok`):
| Field   | Description                                                      |
|:------- |:---------------------------------------------------------------- |
| success | Boolean indicating whether or not the track was found + removed. |

## Events
Events are messages sent from the server to the client indicating some change on the server.

They all share following structure:

| Field  | Value / Description |
|:------ |:------------------- |
| type   | `event` - To distinguish this from the `ok` and `error` messages sent as response to [Commands](#commands) |
| event  | The name of the event (lower camelCase variant of events described below) |
| body   | *Optional* - Additional fields provided by the event (described in corresponding event description below) |

### Add
Indicates that a new [Track](#track) was added to the queue.

**Event body:**

| Field   | Description                          |
|:------- |:------------------------------------ |
| track   | The added [Track](#track)            |
| queue   | The new state of the [Queue](#queue) |

### AddMany
Indicates that more than 1 track was added to the queue at once (typically, this means that a playlist being added).

**Event body:**

| Field   | Description                          |
|:------- |:------------------------------------ |
| queue   | The new state of the [Queue](#queue) |

### Skip (Event)
Indicates that a track was skipped via [Skip](#skip) command.

**Event body:**

| Field        | Description                           |
|:------------ |:------------------------------------- |
| skippedTrack | The skipped [Rich Track](#rich-track) |
| nextTrack    | The next scheduled [Track](#track)    |
| queue        | The new state of the [Queue](#queue)  |

### Next
Emitted when the currently playing track ended playing and the next track is being scheduled to play.

**Event body:**

| Field        | Description                           |
|:------------ |:------------------------------------- |
| track        | The next scheduled [Track](#track)    |
| queue        | The new state of the [Queue](#queue)  |

### End
Emitted when the currently playing track ended playing and the queue is empty.

This event has no body.

### Clear
Indicates that the queue was emptied

This event has no body.

### Remove
Fired when a track was removed from the queue.

**Event body:**

| Field        | Description                                            |
|:------------ |:------------------------------------------------------ |
| track        | The removed [Track](#track)                            |
| index        | The index the track was at in the queue before removal |
| queue        | The new state of the [Queue](#queue)                   |

### Update
Used to signal a change of the current playback state of active [Rich Track](#rich-track).

Fired when:
 - Track is paused / resumed via Spotify-DJ or Spotify program/app
 - A new track starts playing (right after `skip` or `next` event)
 - The currently playing track was overridden via Spotify program/app (manual song selection)
 - Periodically (usually around every 10s)

**Event body:**

| Field        | Description                                            |
|:------------ |:------------------------------------------------------ |
| track        | The new state of the [Rich Track](#rich-track)         |

**Note:** In case of override, the new [Rich Track](#rich-track) might not be the same track as the previously playing one.

## Entities
### Track
| Field    | Description                                                                       |
|:-------- |:--------------------------------------------------------------------------------- |
| type     | Always `track` for tracks. Used to distinguish [playlists](#playlist) from tracks |
| title    | The title (name) of the track as seen in Spotify                                  |
| artists  | Newline seperated list of artists                                                 |
| duration | The duration of the track in ms                                                   |
| album    | The name of the album this track is from                                          |
| img      | Album image url (or `''` if not set). This tries to serve the smalles image >= height of 50px |
| uri      | The Spotify URI used for most operations                                          |

### Rich Track
Track object with additional infos about the playback status.

**Note:** This is an extension of [Track](#track) and contains all of its fields in addition to the fields below.

| Field      | Description                                                             |
|:--------   |:----------------------------------------------------------------------- |
| override   | Boolean indicating whether or not this is playing due to Spotify-DJ playing it (override = `false`) or it being manually played through the Spotify program/app (override = `true`). If `true`, then a call of [Skip](#skip) is required in order for Spotify-DJ continuing to play tracks |
| playing    | Boolean indicating the playback status of the track (playing vs paused) |
| updateTime | The server-timestamp this event was generated at                        |
| seekTime   | The seek time in ms at updateTime                                       |

### Playlist
| Field    | Description                                                                          |
|:-------- |:------------------------------------------------------------------------------------ |
| type     | Always `playlist` for playlists. Used to distinguish playlists from [tracks](#track) |
| name     | The name of the playlist                                                             |
| owner    | The name of the owner (creator) of the playlist                                      |
| img      | Playlist image url (or `''` if not set). This tries to serve the smalles image >= height of 50px |
| uri      | The Spotify URI used for most operations                                             |
| tracks   | The number of total tracks of this playlist                                          |

### Queue
This entity is just an Array of [Track](#track) objects without any fields.