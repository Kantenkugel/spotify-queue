const styles = {
    light: {
        file: 'styles/bootstrap.min.css',
        custom: {
            clearBtn: 'list-group-item-light'
        }
    },
    dark: {
        file: 'styles/bootstrap_darkly.min.css',
        custom: {
            clearBtn: 'active'
        }
    }
};

const defaultStyleKey = 'light';
const localStorageKey = 'selectedStyle';

let selectedStyle = defaultStyleKey;

function setStyle(newStyle) {
    if(!newStyle || !(newStyle in styles))
        return;
    selectedStyle = newStyle;
    storeInStorage();
    applyStyle();
}

let linkElem;
function applyStyle() {
    if(!linkElem) {
        linkElem = document.createElement('link');
        linkElem.rel = 'stylesheet';
        linkElem.type = 'text/css';
        document.querySelector('head').appendChild(linkElem);
    }
    linkElem.href = styles[selectedStyle].file;
}

function loadFromStorage() {
    if(window && 'localStorage' in window) {
        const storageValue = localStorage.getItem(localStorageKey);
        if(storageValue && storageValue in styles)
            selectedStyle = storageValue;
        else
            selectedStyle = defaultStyleKey;
    }
}

function storeInStorage() {
    if(window && 'localStorage' in window) {
        localStorage.setItem(localStorageKey, selectedStyle);
    }
}

module.exports = {
    init() {
        loadFromStorage();
        applyStyle();
    },
    setStyle,
    getStyleName() {
        return selectedStyle;
    },
    getCustomStyles() {
        return styles[selectedStyle].custom;
    }
};