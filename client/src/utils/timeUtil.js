export function formatTime(time) {
    time = Math.floor(time / 1000);
    if(time < 0)
        time = 0;
    const hours = Math.floor(time / 3600);
    const mins = Math.floor((time % 3600) / 60);
    const secs = time % 60;

    return hours > 0 ? `${hours}:${zeroPad(mins)}:${zeroPad(secs)}` : `${mins}:${zeroPad(secs)}`;
}

export function zeroPad(num) {
    if(typeof num === 'string') {
        return (num.length === 1) ? '0'+num : num;
    } else {
        return (num < 10) ? '0'+num : num;
    }
}