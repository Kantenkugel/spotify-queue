import React, { Component } from 'react';
import styleUtil from '../utils/styleConfig';

import '../styles/global.css';
import '../styles/toggle.css';

import CardWrapper from './CardWrapper';

export default class SiteContainer extends Component {
    constructor() {
        super();
        this.state = {
            styles: styleUtil.getCustomStyles(),
            activeStyle: styleUtil.getStyleName()
        };
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-light navbar-expand bg-light">
                    <div className="container justify-content-between">
                        <div className="navbar-brand">Spotify-DJ</div>
                        <div className="navbar-nav">
                            Dark&nbsp;
                            <div className="onoffswitch">
                                <input
                                    type="checkbox"
                                    className="onoffswitch-checkbox"
                                    id="darkmodeSwitch"
                                    checked={this.state.activeStyle === 'dark'}
                                    onChange={(evt) => this._handleStyleChange(evt)}
                                />
                                <label className="onoffswitch-label" htmlFor="darkmodeSwitch"/>
                            </div>
                        </div>
                    </div>
                </nav>
                <CardWrapper styles={this.state.styles}/>
            </div>
        );
    }

    _handleStyleChange(evt) {
        if(evt.target.checked)
            styleUtil.setStyle('dark');
        else
            styleUtil.setStyle('light');
        this.setState({
            styles: styleUtil.getCustomStyles(),
            activeStyle: styleUtil.getStyleName()
        });
    }
}