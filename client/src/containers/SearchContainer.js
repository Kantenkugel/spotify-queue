import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../styles/icons.css';

class SearchContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searched: false
        }
    }

    _getQueueMarker(item) {
        const found = this.props.queue.findIndex(e => e.uri === item.uri);
        return found === -1 ? null : <div>In Queue: #{found+1}</div>;
    }

    render() {
        return (
            <div id="search" className="card">
                <div className="card-header">
                    <form id="searchForm" onSubmit={event => this._search(event)}>
                        <div className="form-row">
                            <div className="col col-md-6 col-lg-4">
                                <label htmlFor="searchInput" className="sr-only">Search</label>
                                <input
                                    id="searchInput"
                                    className="form-control"
                                    type="search"
                                    placeholder="Search"
                                    ref={elem => this._input = elem}
                                />
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-outline-success" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                {this.state.searched && this.props.searchResults && this.props.searchResults.length > 0 && (
                    <ul className="list-group list-group-flush">
                        <button
                            type="button"
                            className={"list-group-item "+this.props.styles.clearBtn}
                            onClick={() => {
                                this._input.value = '';
                                if(this.state.searched) {
                                    this.setState({
                                        searched: false
                                    })
                                }
                            }}
                        >
                            Clear
                        </button>
                        {this.props.searchResults.map((item, index) => (
                            <li key={index} className="list-group-item">
                                <div className="row align-items-center">
                                    <div className="col-auto albumImage">
                                        <img src={item.img} alt="Album" aria-hidden="true" />
                                    </div>
                                    {item.type === 'track' ? (
                                        <div className="col trackInfo">
                                            <div><b>{item.title}</b></div>
                                            <div>{item.artists.replace('\n', ', ')}</div>
                                        </div>
                                    ) : (
                                        <div className="col trackInfo">
                                            <div><b>{item.name}</b> ({item.tracks} tracks)</div>
                                            <div>{item.owner}</div>
                                        </div>
                                    )}
                                    {this._getQueueMarker(item)}
                                    <div className="col-auto">
                                        <button type="button"
                                                className="btn btn-outline-success playerControl rounded-circle"
                                                aria-label="Add"
                                                onClick={() => this.props.onAdd(item.uri)}>
                                            <i className="icon-plus" aria-hidden="true"/>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                )}
                {this.state.searched && this.props.searchResults === undefined && (
                    <div className="card-body">
                        Searching...
                    </div>
                )}
                {this.state.searched && this.props.searchResults && this.props.searchResults.length === 0 && (
                    <div className="card-body">
                        Nothing found!
                    </div>
                )}
            </div>
        );
    }

    _search(event) {
        event.preventDefault();
        const text = this._input.value.trim();
        if(text.length === 0)
            return;
        this._input.blur();
        this.props.onSearch(text);
        this.setState({
            searched: true
        });
    }
}

SearchContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
    searchResults: PropTypes.arrayOf(PropTypes.object),
    queue: PropTypes.arrayOf(PropTypes.object).isRequired,
    styles: PropTypes.object.isRequired
};

export default SearchContainer;