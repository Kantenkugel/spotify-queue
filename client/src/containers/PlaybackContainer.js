import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { formatTime } from '../utils/timeUtil';

import '../styles/icons.css';

class PlaybackContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currTime: Date.now()
        };
    }

    componentDidMount() {
        this._interval = setInterval(() => {
            this.setState({
                currTime: Date.now()
            })
        }, 500);
    }

    componentWillUnmount() {
        if(this._interval)
            clearInterval(this._interval);
        this._interval = false;
    }

    render() {
        const {track, onPlayPause, onSkip, timeDiff} = this.props;
        if(!track) {
            return (
                <div id="player" className="card">
                    <div className="card-body">
                        <div className="row align-items-center">
                            <div className="col">
                                <b>Nothing playing...</b>
                            </div>
                            <div className="col-auto">
                                <div className="btn-group align-middle" role="group">
                                    <button type="button" className="btn btn-outline-info playerControl"
                                            aria-label="play" disabled>
                                        <i className="icon-play" aria-hidden="true"/>
                                    </button>
                                    <button type="button" className="btn btn-outline-danger playerControl"
                                            aria-label="skip" disabled>
                                        <i className="icon-forward" aria-hidden="true"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center">
                            <div className="col">
                                <div className="progress">
                                    <div className="progress-bar bg-success" role="progressbar" style={{width: 0}} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div className="col-auto">
                                {formatTime(0)} / {formatTime(0)}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        const trackTime = track.playing
            ? track.seekTime + (this.state.currTime + timeDiff - track.updateTime)
            : track.seekTime;
        const percentage = Math.floor(trackTime/track.duration*1000)/10;
        return (
            <div id="player" className="card">
                <div className="card-body">
                    <div className="row align-items-center">
                        <div className="col-auto albumImage">
                            <img src={track.img} alt="Album" aria-hidden="true" />
                        </div>
                        <div className="col trackInfo">
                            <div><b>{track.title}</b></div>
                            <div>{track.artists.replace(/\n/g, ', ')}</div>
                        </div>
                        <div className="col-auto">
                            <div className="btn-group align-middle" role="group">
                                <button type="button" className="btn btn-outline-info playerControl"
                                        aria-label={track.playing ? 'pause' : 'play'}
                                        onClick={() => onPlayPause()}>
                                    {track.playing
                                        ? <i className="icon-pause" aria-hidden="true"/>
                                        : <i className="icon-play" aria-hidden="true"/>
                                    }
                                </button>
                                <button type="button" className="btn btn-outline-danger playerControl"
                                        aria-label="skip"
                                        onClick={() => onSkip()}>
                                    <i className="icon-forward" aria-hidden="true"/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col">
                            <div className="progress">
                                <div className="progress-bar bg-success" role="progressbar" style={{width: percentage+'%'}} aria-valuenow={percentage} aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-auto">
                            {formatTime(trackTime)} / {formatTime(track.duration)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

PlaybackContainer.propTypes = {
    onPlayPause: PropTypes.func.isRequired,
    onSkip: PropTypes.func.isRequired,
    track: PropTypes.object,
    timeDiff: PropTypes.number.isRequired
};

export default PlaybackContainer;