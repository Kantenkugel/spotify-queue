import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PlaybackContainer from './PlaybackContainer';
import QueueOverview from '../components/QueueOverview';
import SearchContainer from './SearchContainer';

class CardWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ws: false,
            connected: false,
            connectMsg: 'Connecting...',
            queue: [],
            track: undefined,
            timeDiff: 0,
            searchResults: undefined
        };
    }

    componentDidMount() {
        this._connect();
    }

    refresh() {
        this._send({cmd: 'sync'});
        this._send({cmd: 'get'});
    }

    render() {
        if(this.state.connected) {
            return (
                <div className="container">
                    <PlaybackContainer
                        track={this.state.track}
                        timeDiff={this.state.timeDiff}
                        onPlayPause={this._handleChildEvent.bind(this, 'playpause')}
                        onSkip={this._handleChildEvent.bind(this, 'skip')}
                    />
                    <SearchContainer
                        onSearch={this._handleChildEvent.bind(this, 'search')}
                        onAdd={this._handleChildEvent.bind(this, 'add')}
                        searchResults={this.state.searchResults}
                        queue={this.state.queue}
                        styles={this.props.styles}
                    />
                    <QueueOverview
                        queue={this.state.queue}
                        onDelete={this._handleChildEvent.bind(this, 'remove')}
                        onRefresh={this._handleChildEvent.bind(this, 'refresh')}
                    />
                </div>
            );
        } else {
            return (
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <h3>{this.state.connectMsg}</h3>
                        </div>
                    </div>
                </div>
            );
        }
    }

    _handleChildEvent(event, arg) {
        switch(event) {
            case 'refresh':
                this._send({cmd: 'get'});
                break;
            case 'playpause':
                if(!this.state.track)
                    return;
                this._send({cmd: 'pause', value: this.state.track.playing});
                break;
            case 'skip':
                if(this.state.queue.length === 0)
                    return;
                this._send({cmd: 'skip'});
                break;
            case 'remove':
                if(this.state.queue.length === 0)
                    return;
                if(!arg || !arg.uri || arg.index === undefined)
                    return;
                this._send({
                    cmd: 'remove',
                    uri: arg.uri,
                    index: arg.index
                });
                break;
            case 'search':
                if(!arg)
                    return;
                this.setState({
                    searchResults: undefined
                });
                this._send({cmd: 'search', query: arg});
                break;
            case 'add':
                if(!arg)
                    return;
                this._send({cmd: 'queue', uri: arg});
                break;
            default:
                //NOP
        }
    }

    _onMsg(data) {
        const msg = JSON.parse(data);
        if(msg.type === 'ok') {
            if(msg.cmd === 'get') {
                this.setState({
                    track: msg.body.current,
                    queue: msg.body.queue
                });
            } else if(msg.cmd === 'search') {
                this.setState({
                    searchResults: msg.body
                });
            } else if(msg.cmd === 'sync') {
                this.setState({
                    timeDiff: msg.body - Date.now()
                });
            }
        } else if(msg.type === 'event') {
            switch(msg.event) {
                case 'update':
                    this.setState({
                        track: msg.body.track
                    });
                    break;
                case 'add':
                    if(msg.body.isBulk)
                        break;
                // eslint-disable-next-line
                case 'addMany':
                case 'remove':
                    this.setState({
                        queue: msg.body.queue
                    });
                    break;
                case 'skip':
                case 'next':
                    const track = msg.body.track || msg.body.nextTrack;
                    track.playing = false;
                    track.seekTime = 0;
                    this.setState({
                        track,
                        queue: msg.body.queue
                    });
                    break;
                case 'end':
                    this.setState({
                        track: undefined,
                        queue: []
                    });
                    break;
                case 'clear':
                    this.setState({
                        queue: []
                    });
                    break;
                default:
            }
        } else if(msg.type === 'error') {
            if(msg.cmd === 'search') {
                this.setState({
                    searchResults: []
                });
            } else {
                console.log(msg.error)
            }
        }
    }

    _send(msg) {
        if(!this.state.connected)
            return;
        if(typeof msg === 'object')
            msg = JSON.stringify(msg);
        this.state.ws.send(msg);
    }

    _connect() {
        const addr = window.location.hostname+':81';
        const ws = new WebSocket('ws://' + addr, 'spotifyQueue-1');
        this.setState({
            ws: ws,
            error: false
        });
        this._addListeners(ws);
    }

    _addListeners(ws) {
        ws.addEventListener('open', () => {
            this.setState({
                connected: true
            });
            this.refresh();
        });
        ws.addEventListener('close', (evt) => {
            const newState = {
                ws: false,
                connected: false,
                queue: [],
                track: undefined

            };
            if(this.state.connected) {
                console.log('closed ', evt);
                newState.connectMsg = 'Connection was closed. Attempting to reconnect...';
            }
            setTimeout(() => this._connect(), 2000);
            this.setState(newState);
        });
        ws.addEventListener('message', (event) => {
            this._onMsg(event.data);
        });
    }
}

CardWrapper.propTypes = {
    styles: PropTypes.object.isRequired
};

export default CardWrapper;