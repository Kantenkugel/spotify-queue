import React from 'react';
import ReactDOM from 'react-dom';

import { init as initStyle } from './utils/styleConfig';

import SiteContainer from './containers/SiteContainer';

initStyle();

ReactDOM.render(<SiteContainer/>, document.getElementById('root'));
