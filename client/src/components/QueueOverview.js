import React from 'react';
import PropTypes from 'prop-types';

import '../styles/icons.css';

const QueueOverview = ({queue, onDelete, onRefresh}) => {
    if(queue.length === 0) {
        return (
            <div id="queue" className="card">
                <div className="card-header">
                    <div className="row align-items-center">
                        <div className="col">
                            Queue
                        </div>
                        <div className="col-auto">
                            <button
                                type="button"
                                className="btn btn-sm btn-light rounded-circle"
                                aria-label="refresh"
                                onClick={() => onRefresh()}
                            >
                                <i className="icon-refresh" aria-hidden="true"/>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    No Tracks in Queue
                </div>
            </div>
        );
    }
    return (
        <div id="queue" className="card">
            <div className="card-header">
                <div className="row align-items-center">
                    <div className="col">
                        Queue ({queue.length})
                    </div>
                    <div className="col-auto">
                        <button
                            type="button"
                            className="btn btn-sm btn-light rounded-circle"
                            aria-label="refresh"
                            onClick={() => onRefresh()}
                        >
                            <i className="icon-refresh" aria-hidden="true"/>
                        </button>
                    </div>
                </div>
            </div>
            <ul className="list-group list-group-flush">
                {queue.map((item, index) => (
                    <li key={index} className="list-group-item">
                        <div className="row align-items-center">
                            <div className="col-auto albumImage">
                                <img src={item.img} alt="Album" aria-hidden="true" />
                            </div>
                            <div className="col trackInfo">
                                <div><b>{item.title}</b></div>
                                <div>{item.artists.replace(/\n/g, ', ')}</div>
                            </div>
                            <div className="col-auto">
                                <button type="button" className="btn btn-outline-danger playerControl"
                                        aria-label="remove"
                                        onClick={() => onDelete({uri: item.uri, index})}>
                                    <i className="icon-trash-o" aria-hidden="true"/>
                                </button>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

QueueOverview.propTypes = {
    queue: PropTypes.arrayOf(PropTypes.object).isRequired,
    onDelete: PropTypes.func.isRequired,
    onRefresh: PropTypes.func.isRequired
};

export default QueueOverview;