# Spotify DJ

The web panel, that allows Spotify users to queue songs over the local network!

There is no need to access the PC Spotify is running on or to log into the Spotify app to queue songs.

**Just run the executable on the PC that spotify is running on and you are ready to go\*!** 

This Project's main purpose it for use in parties, where every guest can add his song requests to the queue without too much effort. 

*(\*requires first-time setup)*

## Screenshot
![screenshot](https://i.imgur.com/wHiNYJp.png)

## Setup
 - Download the executable from the [download section](#download) below.
 - Since the executable creates a config file during setup, it is recommended to put it into its own folder
 - Make sure, Spotify is already running
 - Run Spotify-DJ
 - If this is the first time the executable is run (or the config file is missing from the directory its run in), you will be asked to do a simple setup which is required for Spotify's API access (searching for songs).
   - Visit the [Spotify dev portal](https://developer.spotify.com/my-applications/) and create a new Application.
   - You can use any name/description you like for the Application. This is not shown anywhere in Spotify-DJ.
   - Once on the Application page, copy over the `Client ID` and the `Client Secret` to Spotify-DJ.
   - Enter your market ID (country code) into Spotify-DJ. You can find this on [your Spotify Account Overview](https://www.spotify.com/account/overview/). this is used to filter out tracks from search results that are unavailable in your region.
 - Wait for Spotify-DJ to finish connecting
 - Connect to the server by entering the IP shown in the Spotify-DJ window into any device's webbrowser.

## FAQ
 - Do I need to be in the local network of the server to be able to connect?
   - Unless you did some custom configuration on your router to forward the port the server is on, you will need to be in the local network (Wifi / LAN) to be able to connect to the server.
 - Why is the Queue sometimes not worked up? (Spotify playing its next track automatically instead of first track of the Queue)
   - Spotify-DJ only starts the next track, once Spotify has stopped playing. If the currently playing track was started through Spotify itself, chances are very high for Spotify just immediatelly starting up the next track in the current playlist. In order to let Spotify-DJ take over control in those cases, just hit the skip button at the top-right of Spotify-DJ

## Download
It is recommended to get the latest version from the [Release page](https://gitlab.com/Kantenkugel/spotify-queue/tags).

Alternatively, you can check out the [mega.nz folder](https://mega.nz/#F!NjxQwb4L!eY9WCswzIGNwB_zTx0S8eQ) with all the downloads (and possibly pre-releases).

**Note:** Currently i only provide the executable for Windows x64 as I only tested it for that platform. If you would like to try out other platforms, either reach out to me so i can send you a build for that platform to test, or [build Spotify-DJ yourself](#building-it-yourself)

## Changelog
You can find the changelog in [CHANGELOG.md](CHANGELOG.md)

## Building It Yourself
### Prerequisites
To get started, you only need NodeJS (v6.9 or higher) to be installed.

### Setup
*Note: the build files currently included in Spotify-DJ are for Windows only. But you can just take a look inside and execute the commands yourself*

 - First, you need to get Spotify-DJ's source (either clone the repo or download the source zip)
 - Run the `setub.bat` file which initializes all node dependencies for client+server by calling `npm i` in each directory

### Running
To run the server/client, just switch to the corresponding folder and run `npm start`.

This will start them up and watch the files for changes. If any file is changed, the the client/server are restarted (in the case of client even hotswapped).

As the dev-hosted client (on port `3000`) will automatically connect to the server, there is no need to move client files to the server.

### Building
*Note: the build files currently included in Spotify-DJ are for Windows only. But you can just take a look inside and execute the commands yourself*

 - First you might want to take a look at the `package.json` file of the server and change the build target(s) in the `build` script. For available targets, see the [targets section of the pkg package](https://www.npmjs.com/package/pkg#targets)
 - Run the `build.bat` file which will
   - first build the client by issuing the `npm run build` command in the client directory
   - move the built client files to the server's `statics` folder
   - build & package the server by running `npm run build` in the server directory
   - move the packaged executable to the root directory

## Docs
Docs for the WebSocket protocol are available [HERE](docs/Api.md)

## Spotify-DJ's Dependencies
This list serves as an appreciation for all those awesome people writing awesome libs

### Server
 - express - Webserver for serving the client files ([npm](https://npmjs.org/express), [website](http://expressjs.com/))
 - q - Promise library ([npm](https://npmjs.org/q))
 - spotify-web-api-node - Node wrapper for the Spotify Web API ([npm](https://npmjs.org/spotify-web-api-node))
 - spotilocal - Library for interfacing with Spotify ([npm](https://npmjs.org/spotilocal))
 - ws - WebSocket for server<->client communication ([npm](https://npmjs.org/ws))

Dev-Dependencies:
 - jshint - Tool for code checking ([npm](https://npmjs.org/jshint), [website](http://jshint.com/))
 - nodemon - Code-reload for dev ([npm](https://npmjs.org/nodemon))
 - pkg - Build tool to create packaged executables ([npm](https://npmjs.org/pkg))

### Client (React-based Website)
 - create-react-app - Easy-to-use utility for React development ([npm](https://npmjs.org/create-react-app), [website](https://github.com/facebookincubator/create-react-app))
 - react/react-dom - JS lib for creating reactive websites ([npm](https://npmjs.org/react), [website](https://reactjs.org/))
 - prop-types - Used to type-check properties during React development ([npm](https://npmjs.org/prop-types), [website](https://reactjs.org/docs/typechecking-with-proptypes.html))

Other:
 - [Bootstrap](http://getbootstrap.com/) and [Darkly](https://bootswatch.com/darkly/) - for most of the css
 - [FontAwesome](http://fontawesome.io/) in combination with the [IcoMoon App](https://icomoon.io/app) for button icons