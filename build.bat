@echo off
CALL :build_client
CALL :move_statics
CALL :build_server
echo Done
pause
goto:eof

:build_client
echo Building client...
cd client
call npm run build > nul
cd ..
goto:eof

:move_statics
echo Moving output from client to server
rmdir .\server\statics /s /q
mkdir .\server\statics
xcopy .\client\build\* .\server\statics /s /e /i /q /y
goto:eof

:build_server
echo Building server...
cd server
call npm run build > nul
cd ..
move .\server\Spotify-DJ.exe .
goto:eof