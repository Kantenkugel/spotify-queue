@echo off

Call :setup_client
Call :setup_server
echo Everything set up!
pause
goto:eof

:setup_client
echo Setting up client...
cd client
call npm i
cd ..
goto:eof

:setup_server
echo Setting up server...
cd server
call npm i
cd ..
goto:eof