'use strict';

const Q = require('q');
const path = require('path');
const http = require('http');
const express = require('express');

const { getConfig } = require('./js/config');
const { getInterfaces } = require('./js/ip');
const api = require('./js/api');
const Queue = require('./js/playerQueue');
const control = require('./js/playerControl');
const Ws = require('./js/ws');

const queue = new Queue();

let httpServer = false;
let ws = false;
let shutdownCalled = false;

process.on('SIGINT', () => {
    shutdownCalled = true;
    console.log('Received SIGINT... shutting down gracefully');
    if(httpServer)
        httpServer.close();
    if(ws)
        ws.close();
    queue.shutDown();
    
    setTimeout(() => {
        console.log('Did not shut down properly... forcing shutdown');
        process.exit(0);
    }, 30000).unref();
});

process.on('exit', () => {
    console.log('Exiting now');
});

getConfig()
.then((config) => {
    console.log('Getting API token...');
    return api.init(config);
}).then(() => {
    //start local spotify discovery
    console.log('Attempting to connect to local spotify installation...');
    return control.ready();
}).then((status) => {
    if(shutdownCalled)
        return;
    queue.init();
    console.log('Booting up server');
    setupServer();
    ws = new Ws(httpServer, queue);
    httpServer.listen(81, () => {
        console.log('Server is now listening for connections. You can connect via following ips:');
        getInterfaces().forEach((addr) => {
            console.log(addr+':81');
        });
    });
}).catch((err) => {
    console.error(err);
    Q.delay(5000);
});

function setupServer() {
    const app = express();
    const staticsPath = path.join(__dirname, 'statics');
    app.use(express.static(staticsPath));
    app.get('*', (req, res) => {
        res.sendFile(staticsPath+'/index.html');
    });
    app.use((req, res) => {
        res.sendStatus(404);
    });
    app.use((err, req, res, next) => {
        console.error(err.stack);
        res.status(500).send('Something broke!');
    });
    httpServer = http.createServer(app);
}