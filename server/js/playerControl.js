'use strict';

const { Spotilocal } = require('spotilocal');
const spotilocal = new Spotilocal();

const returnOn = ['play', 'pause', 'logout', 'error', 'ap'];
const returnAfterS = 10;

module.exports = {
    ready() {
        return spotilocal.init()
            .then((player) => player.getStatus());
    },
    
    play(track) {
        if(typeof(track) === 'object') {
            if(track.uri) {
                return spotilocal.play(track.uri);
            }
            throw new Error('got track object without uri');
        } else {
            return spotilocal.play(track);
        }
    },
    
    getStatus(poll = false) {
        const prom = poll ? spotilocal.getStatus(returnOn, returnAfterS) : spotilocal.getStatus();
        return prom.then((status) => {
            return {
                playing: status.playing,
                title: status.track.track_resource.name,
                uri: status.track.track_resource.uri,
                progress: status.playing_position,
                volume: status.volume
            };
        }).catch(() => {});
    },
    
    setPause(pause = true) {
        return spotilocal.pause(pause);
    }
};