'use strict';

const os = require('os');

function getInterfaces() {
    const ifaces = os.networkInterfaces();
    const out = [];
    Object.keys(ifaces).forEach((key) => {
        for(let spec of ifaces[key]) {
            if(!spec.internal && spec.family === 'IPv4' && !spec.mac.startsWith('00:')) {
                out.push(spec.address);
                break;
            }
        }
    });
    return out;
}

module.exports = {
    getInterfaces
};