const trackRegex = /^(?:spotify:track:([a-zA-Z0-9]+)|https?:\/\/open.spotify.com\/track\/([a-zA-Z0-9]+))$/;
const playlistRegex = /^(?:spotify:user:([a-zA-Z0-9]+):playlist:([a-zA-Z0-9]+)|https?:\/\/open.spotify.com\/user\/([a-zA-Z0-9]+)\/playlist\/([a-zA-Z0-9]+))$/;

module.exports = {
    extractTrack(input) {
        const match = trackRegex.exec(input);
        if(match)
            return (match[1] || match[2]);
    },
    
    extractPlaylist(input) {
        const match = playlistRegex.exec(input);
        if(match)
            return [match[1] || match[3], match[2] || match[4]];
    }
};