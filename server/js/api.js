'use strict';

const targetImageSize = 50;

const Q = require('q');
const SpotifyWebApi = require('spotify-web-api-node');

const { extractTrack } = require('./extractors');

let spotifyApi;

const options = {};

function parseTracks(data) {
    const arr = [];
    for(let track of data) {
        if(track.is_playable === false)
            continue;
        arr.push({
            type: 'track',
            title: track.name,
            artists: track.artists.reduce((accum, current) => `${accum}\n${current.name}`, '').substring(1),
            duration: track.duration_ms,
            album: track.album.name,
            img: findBestImage(track.album.images),
            uri: track.uri
        });
    }
    return arr;
}

function parsePlaylists(data) {
    const arr = [];
    for(let playlist of data) {
        const tracks = playlist.tracks.items ? parseTracks(playlist.tracks.items.map(item => item.track)) : playlist.tracks.total;
        arr.push({
            type: 'playlist',
            name: playlist.name,
            owner: playlist.owner.display_name,
            img: findBestImage(playlist.images),
            uri: playlist.uri,
            tracks
        });
    }
    return arr;
}

function findBestImage(images) {
    if(images.length === 0)
        return '';
    const filtered = images
            .filter(img => img.height === null || img.height >= targetImageSize)
            .sort((a, b) => a.height === null ? 1 : (b.height === null ? -1 : (a.height - b.height)));
    return filtered[0].url;
}

function auth() {
    return spotifyApi.clientCredentialsGrant()
    .then((data) => {
        //store retrieved api token
        spotifyApi.setAccessToken(data.body.access_token);
        //schedule refresh
        setTimeout(() => auth(), (data.body.expires_in-5)*1000).unref();
    });
}

module.exports = {
    init({clientId, clientSecret, market}) {
        if(spotifyApi)
            return Q.reject(new Error('Already initialized'));
        options.market = market.toUpperCase();
        spotifyApi = new SpotifyWebApi({
            clientId,
            clientSecret
        });
        return auth();
    },
    
    setoption(key, val) {
        options[key] = val;
    },
    
    searchTrack(query) {
        return spotifyApi.searchTracks(query, options)
            .then((data) => parseTracks(data.body.tracks.items));
    },
    
    getTrack(trackId) {
        const trackMatch = extractTrack(trackId);
        if(trackMatch)
            trackId = trackMatch;
        return spotifyApi.getTrack(trackId, options)
            .then((data) => {
                if(data.error)
                    throw new Error(data.error.message);
                if(!data.body.is_playable)
                    throw new Error('Track is not available for current region');
                return parseTracks([data.body])[0];
            });
    },
    
    searchPlaylists(query) {
        return spotifyApi.searchPlaylists(query, options)
            .then((data) => parsePlaylists(data.body.playlists.items));
    },
    
    getPlaylist(userId, playlistId) {
        return spotifyApi.getPlaylist(userId, playlistId, Object.assign({}, options, {fields: 'name,owner.display_name,images(url),uri,tracks'}))
            .then((data) => {
                if(data.error)
                    throw new Error(data.error.message);
                return parsePlaylists([data.body])[0];
            });
    }
};