'use strict';

const configPath = './config.json';

const Q = require('q');
const fs = require('fs');
const readline = require('readline');

function getConfig() {
    return Q.promise((resolve, reject) => {
        console.log('Checking/Reading config file');
        fs.readFile('./config.json', 'utf8', (err, data) => {
            if(err)
                return resolve();
            else
                return resolve(data);
        });
    }).then((config) => {
        if(config) {
            const obj = JSON.parse(config);
            if(!obj.clientId || !obj.clientSecret || !obj.market)
                return Q.reject(new Error('Config file is malformed... try deleting it and relaunching the program'));
            return obj;
        } else {
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
            console.log('Creating new config file...\nPlease visit https://developer.spotify.com/my-applications/ and create an application if you dont have one already.');
            return Q.promise((resolve, reject) => {
                rl.question('please enter your client id ', (appId) => {
                    if(!appId)
                        return reject(new Error('Config was not created'));
                    resolve({clientId: appId});
                });
            }).then((cfg) => {
                return Q.promise((resolve, reject) => {
                    rl.question('please enter your client secret ', (secret) => {
                        if(!secret)
                            return reject(new Error('Config was not created'));
                        cfg.clientSecret = secret;
                        resolve(cfg);
                    });
                });
            }).then((cfg) => {
                return Q.promise((resolve, reject) => {
                    rl.question('please enter your market (account region, visible at https://www.spotify.com/account/overview/ ) ', (market) => {
                        if(!market)
                            return reject(new Error('Config was not created'));
                        cfg.market = market;
                        resolve(cfg);
                    });
                });
            }).then((cfg) => {
                return Q.promise((resolve, reject) => {
                    fs.writeFile('./config.json', JSON.stringify(cfg), (err) => {
                        if(err)
                            return reject(err);
                        console.log('config written to config.json');
                        rl.close();
                        resolve(cfg);
                    });
                });
            }).catch((err) => {
                rl.close();
                throw err;
            });
        }
    });
}

module.exports = {
    getConfig
};