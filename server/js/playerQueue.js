'use strict';

/*
List of emitted events:
queueAdd - Element was added to queue - added track, new queue
queueSkip - Current Element was skipped - skipped track, nextTrack, new queue
queueNext - Track was scheduled due to previous track ending - nextTrack, new queue
queueEnd - current track ended, queue is empty - no args
queueClear - all tracks cleared - no args
queueShutdown - queue was shut down. for consistency, queueClear will be emitted first - no args
queueRemove - removed specific track from queue - removedTrack, removedIndex, new queue
trackUpdate - track was paused/resumed or had seeking changed - track
trackOverride - track was overridden by spotify client - track
error - error playing track - spotify status object with error

Note:
After new track is made active via queueSkip or queueNext events, its state should be assumed paused until a trackUpdate event is received.
trackUpdate and trackOverride events always have the full track object.

track object:
{
    title: STRING,
    artists: STRING(\n delimited list),
    duration: NUMBER(ms),
    album: STRING,
    img: STRING,
    uri: STRING,
    override: OPTIONAL BOOLEAN,
    playing: OPTIONAL BOOLEAN,
    updateTime: OPTIONAL NUMBER(ms timestamp)
    seekTime: OPTIONAL NUMBER (ms)
}

*/

const EventEmitter = require('events');

const Queue = require('./queue');
const {getTrack} = require('./api');
const control = require('./playerControl');

class PlayerQueue extends EventEmitter {
    constructor() {
        super();
        this._queue = new Queue();
    }
    
    init() {
        if(this._queuinitialized)
            return;
        this._queuinitialized = true;
        control.getStatus().then((status) => {
            this._updateStatus(status);
        });
    }
    
    queueMany(tracks) {
        if(this._shutDown || !this._queuinitialized)
            return;
        for(let i=0; i<tracks.length; i++)
            this.queue(tracks[i], true);
        this.emit('queueAddMany', tracks, this.getQueue());
        if(!this._status.playing && this._status.progress === 0)
            this._playNext();
    }
    
    queue(track, bulk = false) {
        if(this._shutDown || !this._queuinitialized)
            return;
        this._queue.push(track);
        this.emit('queueAdd', track, this.getQueue(), bulk);
        if(!bulk) {
            if(!this._status.playing && this._status.progress === 0)
                this._playNext();
        }
    }
    
    skip() {
        if(this._shutDown || !this._queuinitialized)
            return;
        if(this._queue.isEmpty())
            return;
        this._playNext(true);
    }
    
    remove(uri, startIndex) {
        const [deletedIndex, deletedElem] = this._queue.remove((elem) => elem.uri === uri, startIndex);
        if(deletedIndex !== -1) {
            this.emit('queueRemove', deletedElem, deletedIndex, this.getQueue());
            return true;
        }
        return false;
    }
    
    clear() {
        if(this._shutDown || !this._queuinitialized)
            return;
        this._queue.clear();
        this.emit('queueClear');
    }
    
    getCurrentTrack() {
        return this._currentTrack;
    }
    
    getQueue() {
        return this._queue.getArr();
    }
    
    shutDown() {
        this.clear();
        this._shutDown = true;
        this.emit('queueShutdown');
    }
    
    _playNext(skip = false) {
        if(this._queue.isEmpty()) {
            if(this._currentTrack !== undefined) {
                this._currentTrack = undefined;
                this.emit('queueEnd');
            }
            return;
        }
        const nextTrack = this._queue.pop();
        if(skip)
            this.emit('queueSkip', this._currentTrack, nextTrack, this.getQueue());
        else
            this.emit('queueNext', nextTrack, this.getQueue());
        nextTrack.override = false;
        this._currentTrack = nextTrack;
        control.play(this._currentTrack.uri).then((status) => {
            if(status.error) {
                console.error('Skipping track due to some error', status.error);
                this.emit('error', status.error);
                this._playNext();
            }
        }).catch(console.error);
    }
    
    _updateStatus(newStatus) {
        if(this._shutDown)
            return;
        //schedule next fetch
        this._getNewStatus(true);
        if(!newStatus)      //should not happen anymore (using returnAfter) but to not break everything
            return;
        this._status = newStatus;
        const track = this._currentTrack;
        if(!track || (newStatus.uri !== track.uri)) {
            if(!track && !newStatus.playing && newStatus.progress === 0)
                return;         //ignore cases of queue being empty and track spotify returns has ended
            const startTime = Date.now();
            getTrack(newStatus.uri).then((newTrack) => {
                newTrack.override = true;
                newTrack.playing = newStatus.playing;
                newTrack.updateTime = startTime;
                newTrack.seekTime = newStatus.progress * 1000;
                this._currentTrack = newTrack;
                this.emit('trackOverride', this._currentTrack);
            });
            return;
        }
        if(!newStatus.playing && newStatus.progress === 0) {
            //currently not playing anything... queueing next song (if avail)
            this._playNext();
            return;             //do not show update event when queueing next track
        }
        track.playing = newStatus.playing;
        track.updateTime = Date.now();
        track.seekTime = newStatus.progress * 1000;
        this.emit('trackUpdate', track);
    }
    
    _getNewStatus(poll = false) {
        setTimeout(() => control.getStatus(poll).then((status) => {
            this._updateStatus(status);
        }), 100);
    }
}

module.exports = PlayerQueue;