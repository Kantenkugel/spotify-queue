'use strict';

class Queue {
    constructor() {
        this.queue = [];
    }
    
    push(elem) {
        this.queue.push(elem);
    }
    
    pop() {
        return this.queue.shift();
    }
    
    peek() {
        return this.queue[0];
    }
    
    clear() {
        this.queue = [];
    }
    
    remove(elemFilter, startIndex = -1) {
        let foundIndex = -1;
        if(startIndex === -1 || startIndex > this.size()) {
            foundIndex = this.queue.findIndex(elemFilter);
        } else {
            for(let i=startIndex; i>=0; i--) {
                if(elemFilter(this.queue[i])) {
                    foundIndex = i;
                    break;
                }
            }
        }
        if(foundIndex !== -1) {
            return [foundIndex, this.queue.splice(foundIndex, 1)[0]];
        }
        return [-1];
    }
    
    size() {
        return this.queue.length;
    }
    
    isEmpty() {
        return this.size() == 0;
    }
    
    getArr() {
        return this.queue.slice();
    }
}

module.exports = Queue;