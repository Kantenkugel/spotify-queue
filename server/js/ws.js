'use strict';

const protocol = 'spotifyQueue-1';

const WebSocket = require('ws');
const { Server } = require('http');

const { extractTrack, extractPlaylist } = require('./extractors');
const { searchTrack, getTrack, searchPlaylists, getPlaylist } = require('./api');
const PlayerQueue = require('./playerQueue');
const { setPause } = require('./playerControl');

class WS {
    constructor(server, queue) {
        if(!server || !(server instanceof Server))
            throw new Error('No http server provided');
        if(!queue || !(queue instanceof PlayerQueue))
            throw new Error('No playerQueue provided');
        this._queue = queue;
        
        this._server = new WebSocket.Server({
            server,
            handleProtocols(protocols) {
                if(protocols.includes(protocol))
                    return protocol;
                return false;
            },
            maxPayload: 5*1024
        });
        
        this._pingInterval = setInterval(() => {
            this._server.clients.forEach((client) => {
                if (client.isAlive === false) 
                    return client.terminate();

                client.isAlive = false;
                client.ping('', false, true);
            });
        }, 30000).unref();
        
        this._setupListeners();
    }
    
    close() {
        clearInterval(this._pingInterval);
        this._server.close();
    }
    
    broadcast(message) {
        if(typeof message === 'object')
            message = JSON.stringify(message);
        this._server.clients.forEach((client) => {
            if(client.readyState === WebSocket.OPEN)
                client.send(message);
        });
    }
    
    send(client, message) {
        if(typeof message === 'object')
            message = JSON.stringify(message);
        client.send(message);
    }
    
    _handleMessage(client, data) {
        if(typeof data !== 'string') {
            console.log('ignoring non-json (binary) message');
            return this.send(client, 'JSON expected');
        }
        let msg;
        try {
            msg = JSON.parse(data);
        } catch(e) {
            console.log('ignoring non-json message');
            return this.send(client, 'JSON expected');
        }
        if(!msg.cmd)
            return this.send(client, 'Command type expected');
        const respond = this._sendStatus.bind(this, client, msg.cmd);
        switch(msg.cmd) {
            case 'get':
                respond({
                    current: this._queue.getCurrentTrack(),
                    queue: this._queue.getQueue()
                });
                break;
            case 'sync':
                respond(Date.now());
                break;
            case 'search':
                if(!msg.query)
                    respond(new Error('No query provided'));
                if(msg.query.startsWith('pl:')) {
                    searchPlaylists(msg.query.substring(3)).then((playlists) => {
                        respond(playlists);
                    }).catch((err) => {
                        respond(err);
                    });
                    return;
                }
                const trackId = extractTrack(msg.query);
                if(trackId) {
                    getTrack(trackId).then((track) => {
                        respond([track]);
                    }).catch((err) => {
                        respond(err);
                    });
                    return;
                }
                const playlistData = extractPlaylist(msg.query);
                if(playlistData) {
                    getPlaylist(playlistData[0], playlistData[1])
                    .then((playlist) => {
                        playlist.tracks = playlist.tracks.length;
                        respond([playlist]);
                    }).catch((err) => {
                        respond(err);
                    });
                    return;
                }
                searchTrack(msg.query).then((tracks) => {
                    respond(tracks);
                }).catch((err) => {
                    respond(err);
                });
                break;
            case 'queue':
                if(!msg.uri)
                    return respond(new Error('No uri provided'));
                const playlistData2 = extractPlaylist(msg.uri);
                if(playlistData2) {
                    getPlaylist(playlistData2[0], playlistData2[1])
                    .then((playlist) => {
                        this._queue.queueMany(playlist.tracks);
                        playlist.tracks = playlist.tracks.length;
                        respond(playlist);
                    }).catch((err) => {
                        respond(err);
                    });
                    return;
                }
                getTrack(msg.uri).then((track) => {
                    this._queue.queue(track);
                    respond(track);
                }).catch((err) => {
                    respond(err);
                });
                break;
            case 'skip':
                this._queue.skip();
                respond();
                break;
            case 'pause':
                const currTrack = this._queue.getCurrentTrack();
                if(!currTrack)
                    return respond(new Error('No active track'));
                if(msg.value === undefined) {
                    setPause(currTrack.playing);
                } else {
                    setPause(msg.value);
                }
                respond();
                break;
            case 'remove':
                if(!msg.uri || msg.index === undefined)
                    return respond(new Error('Please provide URI and index'));
                const success = this._queue.remove(msg.uri, msg.index);
                respond({success});
                break;
            default:
                respond(new Error('Unknown command'));
        }
    }

    _sendStatus(client, cmd, result) {
        const payload = {
            type: (result && result instanceof Error) ? 'error' : 'ok',
            cmd
        };
        if(result !== undefined) {
            if(result instanceof Error)
                payload.error = result.message;
            else
                payload.body = result;
        }
        this.send(client, payload);
    }
    
    _setupListeners() {
        this._server
            .on('error', this._onError)
            .on('connection', (client) => this._onConnect(client));
        this._queue
            .on('queueAdd', this._broadEvent.bind(this, 'add', ['track', 'queue', 'isBulk']))
            .on('queueAddMany', this._broadEvent.bind(this, 'addMany', ['', 'queue']))
            .on('queueSkip', this._broadEvent.bind(this, 'skip', ['skippedTrack', 'nextTrack', 'queue']))
            .on('queueNext', this._broadEvent.bind(this, 'next', ['track', 'queue']))
            .on('queueEnd', this._broadEvent.bind(this, 'end'))
            .on('queueClear', this._broadEvent.bind(this, 'clear'))
            .on('queueRemove', this._broadEvent.bind(this, 'remove', ['track', 'index', 'queue']))
            .on('trackUpdate', this._broadEvent.bind(this, 'update', ['track']))
            .on('trackOverride', this._broadEvent.bind(this, 'update', ['track']));
    }
    
    _broadEvent(name, keys = [], ...args) {
        if(keys.length !== args.length) {
            console.error('Got mismatching event args... expected '+keys.length+', got '+args.length);
            return;
        }
        const payload = {
            type: 'event',
            event: name
        };
        if(keys.length > 0) {
            payload.body = {};
            for(let index in keys) {
                if(keys[index] !== '')
                    payload.body[keys[index]] = args[index];
            }
        }
        this.broadcast(payload);
    }
    
    _onConnect(client) {
        client.isAlive = true;
        client.on('pong', this._heartbeat.bind(client));
        client.on('message', this._handleMessage.bind(this, client));
    }
    
    _heartbeat() {
        this.isAlive = true;
    }
    
    _onError(error) {
        console.error('There was an error on the ws', error);
    }
}

module.exports = WS;