{
    "body": {
        "tracks": {
            "href": "https://api.spotify.com/v1/search?query=eminem&type=track&market=AT&offset=0&limit=20",
            "items": [{
                    "album": {
                        "album_type": "album",
                        "artists": [{
                                "external_urls": {
                                    "spotify": "https://open.spotify.com/artist/7dGJo4pcD2V6oG8kP0tJRR"
                                },
                                "href": "https://api.spotify.com/v1/artists/7dGJo4pcD2V6oG8kP0tJRR",
                                "id": "7dGJo4pcD2V6oG8kP0tJRR",
                                "name": "Eminem",
                                "type": "artist",
                                "uri": "spotify:artist:7dGJo4pcD2V6oG8kP0tJRR"
                            }
                        ],
                        "available_markets": ["AD", "AR", "AT", "AU", "BE", "BG", "BO", "BR", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "EC", "EE", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IS", "IT", "JP", "LI", "LT", "LU", "LV", "MC", "MT", "MY", "NI", "NL", "NO", "NZ", "PA", "PE", "PH", "PL", "PT", "PY", "SE", "SG", "SK", "SV", "TH", "TR", "TW", "UY", "VN"],
                        "external_urls": {
                            "spotify": "https://open.spotify.com/album/1ftvBBcu7jYIvXyt3JWB8S"
                        },
                        "href": "https://api.spotify.com/v1/albums/1ftvBBcu7jYIvXyt3JWB8S",
                        "id": "1ftvBBcu7jYIvXyt3JWB8S",
                        "images": [{
                                "height": 634,
                                "url": "https://i.scdn.co/image/5f7c90166adfde3170d3341b82c5921d735682a8",
                                "width": 640
                            }, {
                                "height": 297,
                                "url": "https://i.scdn.co/image/e354fc4a159fe2de100dd147a825cd60996901c0",
                                "width": 300
                            }, {
                                "height": 63,
                                "url": "https://i.scdn.co/image/5d811e889a66e94b450ae648ef5801d21ec9cf54",
                                "width": 64
                            }
                        ],
                        "name": "The Eminem Show (Explicit Version)",
                        "type": "album",
                        "uri": "spotify:album:1ftvBBcu7jYIvXyt3JWB8S"
                    },
                    "artists": [{
                            "external_urls": {
                                "spotify": "https://open.spotify.com/artist/7dGJo4pcD2V6oG8kP0tJRR"
                            },
                            "href": "https://api.spotify.com/v1/artists/7dGJo4pcD2V6oG8kP0tJRR",
                            "id": "7dGJo4pcD2V6oG8kP0tJRR",
                            "name": "Eminem",
                            "type": "artist",
                            "uri": "spotify:artist:7dGJo4pcD2V6oG8kP0tJRR"
                        }, {
                            "external_urls": {
                                "spotify": "https://open.spotify.com/artist/1Oa0bMld0A3u5OTYfMzp5h"
                            },
                            "href": "https://api.spotify.com/v1/artists/1Oa0bMld0A3u5OTYfMzp5h",
                            "id": "1Oa0bMld0A3u5OTYfMzp5h",
                            "name": "Nate Dogg",
                            "type": "artist",
                            "uri": "spotify:artist:1Oa0bMld0A3u5OTYfMzp5h"
                        }
                    ],
                    "available_markets": ["AD", "AR", "AT", "AU", "BE", "BG", "BO", "BR", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "EC", "EE", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IS", "IT", "JP", "LI", "LT", "LU", "LV", "MC", "MT", "MY", "NI", "NL", "NO", "NZ", "PA", "PE", "PH", "PL", "PT", "PY", "SE", "SG", "SK", "SV", "TH", "TR", "TW", "UY", "VN"],
                    "disc_number": 1,
                    "duration_ms": 297893,
                    "explicit": true,
                    "external_ids": {
                        "isrc": "USIR10211066"
                    },
                    "external_urls": {
                        "spotify": "https://open.spotify.com/track/6yr8GiTHWvFfi4o6Q5ebdT"
                    },
                    "href": "https://api.spotify.com/v1/tracks/6yr8GiTHWvFfi4o6Q5ebdT",
                    "id": "6yr8GiTHWvFfi4o6Q5ebdT",
                    "name": "'Till I Collapse",
                    "popularity": 80,
                    "preview_url": null,
                    "track_number": 18,
                    "type": "track",
                    "uri": "spotify:track:6yr8GiTHWvFfi4o6Q5ebdT"
                },
                ...
            ],
            "limit": 20,
            "next": "https://api.spotify.com/v1/search?query=eminem&type=track&market=AT&offset=20&limit=20",
            "offset": 0,
            "previous": null,
            "total": 4167
        }
    },
    "headers": {
        "server": "nginx",
        "date": "Mon, 18 Dec 2017 13:56:11 GMT",
        "content-type": "application/json; charset=utf-8",
        "transfer-encoding": "chunked",
        "connection": "close",
        "cache-control": "public, max-age=7200",
        "access-control-allow-origin": "*",
        "access-control-allow-methods": "GET, POST, OPTIONS, PUT, DELETE",
        "access-control-allow-credentials": "true",
        "access-control-max-age": "604800",
        "access-control-allow-headers": "Accept, Authorization, Origin, Content-Type",
        "content-encoding": "gzip",
        "x-content-type-options": "nosniff",
        "strict-transport-security": "max-age=31536000"
    },
    "statusCode": 200
}
